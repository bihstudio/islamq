package m.IslamQ.Settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Mersad on 1/18/2018.
 */
interface IQSettingsConstants {
    String kSettings = "settings";
    String kSound = "sound_enabled";

}
public class IQSettings implements IQSettingsConstants {
    private static IQSettings singleton = new IQSettings();
    public Context context = null;
    private Boolean isSoundEnabled = true;

    private IQSettings() {
    }

    /* Static 'instance' method */
    public static IQSettings getInstance() {
        if (singleton == null) {
            singleton = new IQSettings();
            Log.d("GameHelper", "Settings allocated");
        }
        return singleton;
    }


    public void loadSettings() {
        SharedPreferences settings = context.getSharedPreferences(IQSettingsConstants.kSettings, 0);
        isSoundEnabled = settings.getBoolean(IQSettingsConstants.kSound, true);

    }

    public Boolean getSoundEnabled() {
        return isSoundEnabled;
    }

    public void setSoundEnabled(Boolean soundEnabled) {
        SharedPreferences.Editor settingsEditor = context.getSharedPreferences(IQSettingsConstants.kSettings, 0).edit();
        settingsEditor.putBoolean(IQSettingsConstants.kSound, soundEnabled);
        settingsEditor.commit();
        isSoundEnabled = soundEnabled;
    }

}
