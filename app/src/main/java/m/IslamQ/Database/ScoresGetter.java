package m.IslamQ.Database;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import m.IslamQ.Items.Score;

/**
 * Created by Enver on 16.02.2017..
 */

public class ScoresGetter extends DataBaseHelper<Score> {

    public ScoresGetter(String networkKey) {
        super(networkKey);
    }

    @Override
    protected String DataToSend() {
        return "?" + "action" + "=" + "getHighScores"
                + "&" + "key" + "=" + networkKey;
    }

    @Override
    Score ObjectToData(JSONObject jsonObject, int index) {
        try
        {
            String playerName = jsonObject.getString("name");
            int points = jsonObject.getInt("score");
            String fbUserId = jsonObject.getString("fb_userid");

            if (fbUserId.equals("null"))
                fbUserId = null;

            return new Score(playerName, index + 1, points, fbUserId);
        } catch (JSONException e) {
            Log.e("Parse", "Error parsing scores!");
        }

        return null;
    }
}
