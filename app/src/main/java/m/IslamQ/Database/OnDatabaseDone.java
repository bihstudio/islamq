package m.IslamQ.Database;

/**
 * Created by Enver on 16.02.2017..
 */

public interface OnDatabaseDone<T> {
    void OnDatabaseGetData(T data);
}