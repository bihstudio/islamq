package m.IslamQ.Database;

import android.graphics.Bitmap;

/**
 * Created by Enver on 04.09.2017..
 */

public interface OnImageGetListener {

    void OnImageGet(Bitmap bitmap);
}
