package m.IslamQ.Database;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Enver on 16.02.2017..
 */

public class ScoreSetter extends DataBaseHelper<Boolean> {

    private String name;
    private int score;
    private String fbUserId;

    public ScoreSetter(String networkKey, String name, int score, String fbUserId) {
        super(networkKey);
        this.name = name;
        this.score = score;
        this.fbUserId = fbUserId;
    }

    @Override
    protected String DataToSend() {
        return "?" + "action" + "=" + "setScore"
                + "&" + "key" + "=" + networkKey
                + "&" + "name" + "=" + name.replaceAll(" ", "%20")
                + "&" + "score" + "=" + score
                + "&" + "fbUserId" + "=" + fbUserId;
    }

    @Override
    List<Boolean> ParseData(String result) {
        List<Boolean> data = new ArrayList<>();
        data.add(0, result.contains("Added"));
        return data;
    }

    @Override
    Boolean ObjectToData(JSONObject jsonObject, int index) {
        return null;
    }
}