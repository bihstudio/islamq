package m.IslamQ.Database;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import m.IslamQ.Items.Question;

/**
 * Created by Enver on 11.02.2017..
 */

public abstract class DataBaseHelper<T> extends AsyncTask<OnDatabaseDone, Void, List<Question>> {

    protected String networkKey;

    DataBaseHelper(String networkKey) { this.networkKey = networkKey; }

    @Override
    protected List<Question> doInBackground(OnDatabaseDone... onDatabaseDone) {
        String result = "";
        InputStream isr;

        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://quizquestions.atwebpages.com/hidden/iqquiz.php" + DataToSend()); //YOUR PHP SCRIPT ADDRESS
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            isr = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(isr));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
                sb.append(line + "\n");
            isr.close();
            result=sb.toString();
        }catch(Exception e){
            Log.e("log_tag", "Error in http connection "+e.toString());
        }

        List<T> data = ParseData(result);

        if(onDatabaseDone[0] != null)
            onDatabaseDone[0].OnDatabaseGetData(data);

        return null;
    }

    List<T> ParseData(String result)
    {
        List<T> data = new ArrayList<>();

        try {
            JSONArray jArray = new JSONArray(result);
            for(int i=0; i<jArray.length();i++){
                JSONObject json = jArray.getJSONObject(i);
                data.add(ObjectToData(json, i));
            }
        } catch (Exception e) {
            Log.e("log_tag", "Error Parsing Data "+e.toString());
        }

        return data;
    }

    abstract String DataToSend();
    abstract T ObjectToData(JSONObject jsonObject, int index);
}