package m.IslamQ.Items;

import android.graphics.Bitmap;

import m.IslamQ.Database.OnImageGetListener;
import m.IslamQ.Utils;

/**
 * Created by Enver on 15.02.2017..
 */

public class Score {
    public String playerName;
    public int rank;
    public int points;
    public String fbUserId;
    public Bitmap profilePic;

    private OnImageGetListener onImageGetListener = null;

    public Score(String playerName, int rank, int points, String fbUserId) {
        this.playerName = playerName;
        this.rank = rank;
        this.points = points;
        this.fbUserId = fbUserId;

        if (fbUserId != null)
            Utils.getFacebookProfilePicture(fbUserId + "", new OnImageGetListener() {
                @Override
                public void OnImageGet(Bitmap bitmap) {
                    profilePic = bitmap;

                    if (onImageGetListener != null) {
                        onImageGetListener.OnImageGet(bitmap);

                        onImageGetListener = null;
                    }
                }
            });
    }

    public void setOnImageGetListener(OnImageGetListener onImageGetListener) {
        this.onImageGetListener = onImageGetListener;
    }
}
