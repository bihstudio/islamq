package m.IslamQ.Items;

import com.google.android.gms.wallet.WalletConstants;

/**
 * Created by Mersad on 9/7/2017.
 */

 public  class Constants {

    public static final String AMOUNT = "1.00";
    public static final String CURRENCY_CODE = "USD";

    public static final int WALLET_ENVIRONMENT = WalletConstants.ENVIRONMENT_TEST;
}
