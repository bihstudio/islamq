package m.IslamQ.Items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Question implements Serializable{

	private int id;
	private String questionText = "";
	private String rightText = "";
	private String[] wrongTexts = new String[3];

	public Question(int id, String questionText, String rightText, String wrongText1, String wrongText2, String wrongText3) {
		this.id = id;
		this.questionText = questionText;
		this.rightText = rightText;
		this.wrongTexts = new String[] {wrongText1, wrongText2, wrongText3};
	}

	//Randomized texts
	private List<String> texts;

	//Index of button of right answer [0..3]
	private int rightAnswer;

	//Gets randomized text for buttons
	public String[] Randomize()
	{
		Random random = new Random();
		rightAnswer = random.nextInt(4);

		texts = new ArrayList<>();
		Collections.addAll(texts, wrongTexts);

		Collections.shuffle(texts, random);

		texts.add(rightAnswer, rightText);

		return texts.toArray(new String[texts.size()]);
	}

	public int getRightAnswer() {
		return rightAnswer;
	}
	public String getQuestionText() {
		return questionText;
	}
	public int getId() {
		return id;
	}
	public String getRightText() {
		return rightText;
	}
	public String[] getWrongTexts() {
		return wrongTexts;
	}
}