package m.IslamQ.Items;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import m.IslamQ.R;


/**
 * Created by mersad on 9.8.2017.
 */

public class QuestionRightShowAdapter extends RecyclerView.Adapter<QuestionRightShowAdapter.MyViewHolder> {

    private List<Question> itemList;
    Context context;

    public QuestionRightShowAdapter(Context context,List<Question> mItemList) {
        this.itemList = mItemList;
        this.context = context;
    }

    @Override
    public QuestionRightShowAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_show, parent, false);

        return new QuestionRightShowAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionRightShowAdapter.MyViewHolder holder, int position) {
        Question item = itemList.get(position);
        holder.question.setText(item.getQuestionText() + "");
        holder.trueAnswer.setText(item.getRightText() + "");
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView  question, trueAnswer;

        public MyViewHolder(View view) {
            super(view);
            question = (TextView) view.findViewById(R.id.question);
            trueAnswer = (TextView) view.findViewById(R.id.trueAnswer);

        }
    }
}


