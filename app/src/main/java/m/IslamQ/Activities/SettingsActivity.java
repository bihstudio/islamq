package m.IslamQ.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.facebook.login.LoginManager;

import m.IslamQ.R;
import m.IslamQ.Settings.IQSettings;

public class SettingsActivity extends Activity {
    Switch soundSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        soundSwitch = findViewById(R.id.sound_switch);
        Button logoutButton = (Button) findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove("name");
                editor.commit();
                Intent intent = new Intent(SettingsActivity.this, LoginPage.class);
                startActivity(intent);
                finish();
            }
        });
        soundSwitch.setChecked(IQSettings.getInstance().getSoundEnabled());
        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                IQSettings.getInstance().setSoundEnabled(soundSwitch.isChecked());
            }
        });
    }

}
