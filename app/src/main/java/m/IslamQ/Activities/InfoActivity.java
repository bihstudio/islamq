package m.IslamQ.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

import m.IslamQ.Database.OnDatabaseDone;
import m.IslamQ.Database.QuestionGetter;
import m.IslamQ.Items.Question;
import m.IslamQ.Items.QuestionRightShowAdapter;
import m.IslamQ.R;

/**
 * Created by mersad on 8.1.2017.
 */

public class InfoActivity extends Activity implements OnDatabaseDone<List<Question>> {

    public List<Question> itemList = new ArrayList<>();
    protected RecyclerView recyclerView;
    protected QuestionRightShowAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopage);

        recyclerView = findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout)  findViewById(R.id.swipeRefreshLayout_info);

        mAdapter = new QuestionRightShowAdapter(this,itemList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        new QuestionGetter(QuestionActivity.NETWORK_KEY).execute(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                refreshContent();
            }
        });
    }
    @Override
    public void OnDatabaseGetData(List<Question> data) {
        for (Question item : data)
            itemList.add(item);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
    public  void  refreshContent(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                mAdapter = new QuestionRightShowAdapter(InfoActivity.this,itemList);
                recyclerView.setLayoutManager(new LinearLayoutManager(InfoActivity.this));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
        swipeRefreshLayout.setRefreshing(false);
    }
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}