package m.IslamQ.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import m.IslamQ.R;


public class SplashActivity extends Activity {
    // SplashActivity screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginPage.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}

