package m.IslamQ.Activities;

/**
 * Created by H on 7/12/2015.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.Profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import m.IslamQ.App;
import m.IslamQ.CountDownProgress;
import m.IslamQ.Database.OnDatabaseDone;
import m.IslamQ.Database.ScoreSetter;
import m.IslamQ.Items.Question;
import m.IslamQ.QuizHelper;
import m.IslamQ.R;
import m.IslamQ.Services.BackGroundMusicService;
import m.IslamQ.Settings.IQSettings;
import m.IslamQ.Utilities.IQUtilities;

@SuppressLint("RestrictedApi")
public class QuestionActivity extends Activity implements OnDatabaseDone<List<Boolean>>, View.OnClickListener {

    public final static String NETWORK_KEY = "evaiIwPbcJpAX88";

    QuizHelper quizHelper;

    //Quiz stats
    Question currentQ;
    int score = 0;
    int mProgressStatus = 0;
    boolean progressInterrupt = false;
    private boolean bPolaUsed = false, bPozivUsed = false, bZamjenaUsed = false;
    //Views
    private TextView txtQuestion, scored;
    private AppCompatButton button1, button2, button3, button4;
    private ImageButton bpola, bpoziv, bzamjena;
    private CountDownProgress mProgress;
    private AlertDialog dialog;
    private volatile Thread progressThread;
    private Handler handler;
    private boolean pauseTimerForAnswerShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        IQUtilities.getInstance().setContext(getApplicationContext());
        //Find views
        txtQuestion = findViewById(R.id.txtQuestion);
        button1 = findViewById(R.id.answer_one);
        button2 = findViewById(R.id.answer_two);
        button3 = findViewById(R.id.answer_three);
        button4 = findViewById(R.id.answer_four);
        bpola = findViewById(R.id.bPola);
        bpoziv = findViewById(R.id.bPoziv);
        bzamjena = findViewById(R.id.bZamjena);
        mProgress = findViewById(R.id.progress_view);
        scored = findViewById(R.id.score);

        //Personalize
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/droid.ttf");
        txtQuestion.setTypeface(type);
        button1.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        button2.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        button3.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        button4.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        // bpola.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        //bpoziv.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        //bzamjena.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);

        handler = new Handler();

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        bpola.setOnClickListener(this);
        bpoziv.setOnClickListener(this);
        bzamjena.setOnClickListener(this);

        //Prepare questions
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        ArrayList<Question> questions = (ArrayList<Question>) args.getSerializable("ARRAYLIST");
        quizHelper = new QuizHelper(questions, this);

        //Start the game
        startTheTimer();
        SetQuestion();
    }

    private void resetGameAndStart() {
        bPolaUsed = false;
        bPozivUsed = false;
        bZamjenaUsed = false;
        // bpola.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        // bpoziv.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        //bzamjena.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        bpola.setAlpha(1);
        bpola.setAlpha(1);
        bpola.setAlpha(1);
        UpdateScore(0);
        SetQuestion();
        mProgressStatus = 0;
        progressInterrupt = false;
        startTheTimer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answer_one:
                Answer(0, true);

                break;
            case R.id.answer_two:
                Answer(1, true);
                break;
            case R.id.answer_three:
                Answer(2, true);
                break;
            case R.id.answer_four:
                Answer(3, true);
                break;
            case R.id.bPola:
                IQUtilities.getInstance().playTapSound();
                bPolaUsed = true;
                HelpHalf();
                // bpola.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bpola.setEnabled(false);
                break;
            case R.id.bPoziv:
                IQUtilities.getInstance().playTapSound();
                bPozivUsed = true;
                LeaveOnlyRightAnswer();
                //bpoziv.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bpoziv.setEnabled(false);
                break;
            case R.id.bZamjena:
                IQUtilities.getInstance().playTapSound();
                bZamjenaUsed = true;
                SkipQuestion();
                //bzamjena.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bzamjena.setEnabled(false);
                break;
        }
    }

    private void LeaveOnlyRightAnswer() {

        int a = currentQ.getRightAnswer();

        if (a != 0) {
            button1.setText("");
            button1.setEnabled(false);
        }
        if (a != 1) {
            button2.setText("");
            button2.setEnabled(false);
        }
        if (a != 2) {
            button3.setText("");
            button3.setEnabled(false);
        }
        if (a != 3) {
            button4.setText("");
            button4.setEnabled(false);
        }
    }

    private void HelpHalf() {
        int a = currentQ.getRightAnswer();

        Random random = new Random();
        int b = random.nextInt(4);
        if (a == b)
            b++;
        if (b == 4)
            b = 0;

        if (a != 0 && b != 0)
            button1.setText("");
        if (a != 1 && b != 1)
            button2.setText("");
        if (a != 2 && b != 2)
            button3.setText("");
        if (a != 3 && b != 3)
            button4.setText("");
    }

    private void SkipQuestion() {
        Answer(currentQ.getRightAnswer(), false);
    }

    private void startTheTimer() {
        progressThread = new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus < 20 && !progressInterrupt) {
                    if (!pauseTimerForAnswerShowing) {
                        mProgressStatus++;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgress.setProgress(mProgressStatus);
                            }
                        });
                    }
                    try {
                        Thread.sleep(1000);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if (!progressInterrupt)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Answer(-1, false);
                        }
                    });
            }
        });
        progressInterrupt = false;
        progressThread.start();
    }

    @Override
    public void onBackPressed() {
        if (mProgressStatus < 20) {
            mProgressStatus = 20;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgress.setProgress(mProgressStatus);
                }
            });
            progressInterrupt = true;
            GameOver();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void OnDatabaseGetData(List<Boolean> data) {
        //*Score saved to base*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    dialog = new AlertDialog.Builder(QuestionActivity.this).create();
                    dialog.setMessage("Sačuvano!");
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Nova igra",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    resetGameAndStart();
                                }
                            });
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Glavni menu",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                    dialog.show();
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void SetQuestion() {
        currentQ = quizHelper.GetRandomQuestion();

        if (currentQ != null)
            setQuestionViews();
        else {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setMessage("Čestitamo, odgovorili ste na sva pitanja!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                GameOver();
                            }
                        });
                alertDialog.show();
            } catch (WindowManager.BadTokenException e) {
                e.printStackTrace();
            }
        }
    }

    public void Answer(int answeredId, boolean increaseScore) {
        AppCompatButton button;
        switch (answeredId) {
            case 0:
                button = button1;
                break;
            case 1:
                button = button2;
                break;
            case 2:
                button = button3;
                break;
            default:
                button = button4;
                break;
        }

        AppCompatButton rightButton;
        switch (currentQ.getRightAnswer()) {
            case 0:

                rightButton = button1;

                break;
            case 1:
                rightButton = button2;
                break;
            case 2:
                rightButton = button3;

                break;
            default:
                rightButton = button4;

                break;
        }

        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
        button4.setEnabled(false);
        bpola.setEnabled(false);
        bpoziv.setEnabled(false);
        bzamjena.setEnabled(false);

        pauseTimerForAnswerShowing = true;

        if (currentQ.getRightAnswer() == answeredId) {
            IQUtilities.getInstance().playCorrectAnswerSound();
            button.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.right_answer)));
            if (increaseScore)
                UpdateScore(score + 1);

            mProgressStatus = 0;

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    SetQuestion();
                }
            }, 800);
        } else if (answeredId == -1) {
            rightButton.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.right_answer)));

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GameOver();
                }
            }, 800);
        } else {
            rightButton.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.right_answer)));
            button.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.false_answer)));
            IQUtilities.getInstance().playWrongAnswerSound();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GameOver();
                }
            }, 800);
        }
    }

    public void GameOver() {
        //Stop the timer
        progressInterrupt = true;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    dialog = new AlertDialog.Builder(QuestionActivity.this).create();
                    dialog.setMessage("Tačan odgovor je: " + currentQ.getRightText());
                    dialog.setTitle("Ukupan broj bodova: " + score);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Nova igra",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    resetGameAndStart();
                                }
                            });
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Glavni menu",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });

                    if (score > 0)
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Sačuvaj",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        String fbUserId = Profile.getCurrentProfile().getId();

                                        if (fbUserId == null || fbUserId.isEmpty())
                                            fbUserId = "";

                                        ScoreSetter scoreSetter = new ScoreSetter(NETWORK_KEY, App.getInstance().name, score, fbUserId);
                                        scoreSetter.execute(QuestionActivity.this);
                                    }
                                });
                    dialog.show();
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void UpdateScore(int score) {
        this.score = score;
        scored.setText("" + score);
    }

    private void setQuestionViews() {
        txtQuestion.setText(currentQ.getQuestionText());

        String[] texts = currentQ.Randomize();

        button1.setText(texts[0]);
        button2.setText(texts[1]);
        button3.setText(texts[2]);
        button4.setText(texts[3]);

        button1.setEnabled(true);
        button2.setEnabled(true);
        button3.setEnabled(true);
        button4.setEnabled(true);

        button1.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        button2.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        button3.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        button4.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));

        bpola.setEnabled(!bPolaUsed);
        bpoziv.setEnabled(!bPozivUsed);
        bzamjena.setEnabled(!bZamjenaUsed);

        //Make sure pause is not active
        pauseTimerForAnswerShowing = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (IQSettings.getInstance().getSoundEnabled()) {
            Intent svc = new Intent(this, BackGroundMusicService.class);
            stopService(svc);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        progressThread.interrupt();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IQSettings.getInstance().getSoundEnabled()) {
            Intent svc = new Intent(this, BackGroundMusicService.class);
            startService(svc);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progressThread.interrupt();
        finish();
    }
}

