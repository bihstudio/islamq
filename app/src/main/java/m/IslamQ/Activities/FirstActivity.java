package m.IslamQ.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import m.IslamQ.Items.Question;
import m.IslamQ.QuizHelper;
import m.IslamQ.R;
import m.IslamQ.Settings.IQSettings;
import m.IslamQ.Utilities.IQUtilities;

/**
 * Created by husic on 26.11.2016..
 */
public class FirstActivity extends Activity implements View.OnClickListener {

    static ImageButton playButton;
    ImageButton scoreButton, infoButton, settingsButton;
    QuizHelper quizHelper;
    Thread threadfirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IQSettings settings = IQSettings.getInstance();
        settings.context = getApplicationContext();
        settings.loadSettings();
        setContentView(R.layout.firstactivity);

        IQUtilities.getInstance().setContext(getApplicationContext());

        playButton = findViewById(R.id.playButton);
        scoreButton = findViewById(R.id.scoreButton);
        infoButton = findViewById(R.id.infoButton);
        settingsButton = findViewById(R.id.settingsButton);

        quizHelper = new QuizHelper(QuestionActivity.NETWORK_KEY, this);

        playButton.setOnClickListener(this);
        scoreButton.setOnClickListener(this);
        infoButton.setOnClickListener(this);
        settingsButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playButton:
                //Quiz should start, so to fetch questions we need some time, and we
                //make spinner to fill the space, and then we catch the questions
                IQUtilities.getInstance().playTapSound();
                final ProgressDialog nDialog = new ProgressDialog(FirstActivity.this);
                nDialog.setMessage("Loading..");
                nDialog.setTitle("Getting Data");
                nDialog.setIndeterminate(true);
                nDialog.setCancelable(false);
                nDialog.setCanceledOnTouchOutside(false);
                nDialog.show();
                threadfirst = new Thread(new Runnable() {
                    public void run() {
                        while (!quizHelper.questionReady && !quizHelper.questionError)
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {

                            }

                        if (!quizHelper.questionError) {
                            Intent intent = new Intent(FirstActivity.this, QuestionActivity.class);

                            ArrayList<Question> questions = new ArrayList<>();
                            questions.addAll(quizHelper.questions);
                            Bundle args = new Bundle();
                            args.putSerializable("ARRAYLIST", questions);
                            intent.putExtra("BUNDLE", args);
                            startActivity(intent);
                            nDialog.dismiss();
                        } else
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(FirstActivity.this, "No questions. Please connect to network!", Toast.LENGTH_LONG).show();
                                }
                            });
                    }
                });

                threadfirst.start();
                break;
            case R.id.scoreButton:
                IQUtilities.getInstance().playTapSound();
                Intent scoreActivity = new Intent(FirstActivity.this, HighScore.class);
                startActivity(scoreActivity);
                break;
            case R.id.infoButton:
                IQUtilities.getInstance().playTapSound();
                Intent infoActivity = new Intent(FirstActivity.this, InfoActivity.class);
                startActivity(infoActivity);
                break;
            case R.id.settingsButton:
                IQUtilities.getInstance().playTapSound();
                Intent settingsActivity = new Intent(FirstActivity.this, SettingsActivity.class);
                startActivity(settingsActivity);
                break;
        }
    }

    @Override
    protected void onStop() {
        if (threadfirst != null)
            threadfirst.interrupt();
        super.onStop();
    }
}
