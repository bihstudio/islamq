package m.IslamQ.Utilities;

import android.content.Context;
import android.media.MediaPlayer;

import m.IslamQ.R;
import m.IslamQ.Settings.IQSettings;

/**
 * Created by Mersad on 1/10/2018.
 */

public class IQUtilities {
    private static IQUtilities singleton = new IQUtilities();
    Context context;

    private IQUtilities() {
    }

    /* Static 'instance' method */
    public static IQUtilities getInstance() {
        return singleton;
    }




    public void setContext(Context context) {
        this.context = context;
    }

    public void playCorrectAnswerSound() {
        if (IQSettings.getInstance().getSoundEnabled()) {
            MediaPlayer mp;
            mp = MediaPlayer.create(context, R.raw.right);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.reset();
                    mp.release();
                }
            });
            mp.start();
        }
    }

    public void playWrongAnswerSound() {
        if (IQSettings.getInstance().getSoundEnabled()) {
            MediaPlayer mp;
            mp = MediaPlayer.create(context, R.raw.wrong);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.reset();
                    mp.release();
                }
            });
            mp.start();
        }
    }

    public void playTapSound() {
        if (IQSettings.getInstance().getSoundEnabled()) {
            MediaPlayer mp;
            mp = MediaPlayer.create(context, R.raw.tap);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.reset();
                    mp.release();
                }
            });
            mp.start();
        }
    }



}
