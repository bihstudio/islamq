package m.IslamQ;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import m.IslamQ.Database.OnDatabaseDone;
import m.IslamQ.Database.QuestionGetter;
import m.IslamQ.Items.Question;

public class QuizHelper implements OnDatabaseDone<List<Question>>, Serializable {

    public List<Question> questions;
    public boolean questionReady = false;
    public boolean questionError = false;
    private Random random;
    private LocalBaseHelper localBaseHelper;

    public QuizHelper(String networkKey, Context context) {
        localBaseHelper = new LocalBaseHelper(context);

        QuestionGetter questionGetter = new QuestionGetter(networkKey, localBaseHelper.getLastId());
        questionGetter.execute(this);

        random = new Random();
    }

    public QuizHelper(ArrayList questions, Context context) {
        localBaseHelper = new LocalBaseHelper(context);
        this.questions = questions;

        random = new Random();
    }

    public Question GetRandomQuestion()
    {
        if(questions.size() > 0) {
            int nextQ = random.nextInt(questions.size());
            Question question = questions.get(nextQ);

            localBaseHelper.setQuestionAnswered(question.getId());
            questions.remove(question);
            return question;
        } else {
            localBaseHelper.ClearBase();
            return null;
        }
    }

    @Override
    public void OnDatabaseGetData(List<Question> questions) {
        if(questions != null && questions.size() > 0)
            localBaseHelper.addQuestions(questions);

        this.questions = localBaseHelper.getLocalQuestions();

        if(this.questions != null && this.questions.size() > 0)
            questionReady = true;
        else
            questionError = true;
    }
}